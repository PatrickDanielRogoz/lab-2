package rogoz.patrick.lab2.Ex4MaxInVector;

import java.util.Scanner;


class Ex4MaxInVector {
    public static void main(String[] args)
    {
        Scanner in=new Scanner(System.in);

        int[] v=new int [10];
        System.out.println("n= ");
        int n=in.nextInt();
        int max=0;
        for (int i=0;i<n;i++) {
            System.out.println("V[" + i + "]=");
            v[i] = in.nextInt();
            if (v[i]>max)
                max=v[i];
        }
        System.out.println("Maxim="+max);
    }
}

