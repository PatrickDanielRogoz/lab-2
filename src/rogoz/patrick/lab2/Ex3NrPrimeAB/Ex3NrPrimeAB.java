package rogoz.patrick.lab2.Ex3NrPrimeAB;

import java.util.Scanner;

public class Ex3NrPrimeAB {

    public static void main(String[] args)
    {
        int aux=0;
        int nr=0;
        Scanner in=new Scanner(System.in);
        System.out.println("A=");
        int A=in.nextInt();
        System.out.println("B=");
        int B=in.nextInt();

        for (int i=A;i<=B;i++)
        {
            aux=0;
            for (int j=1;j<=i;j++)
                if (i%j == 0)
                    aux++;
            if (aux == 2) {
                nr++;
                System.out.println(i + " ");
            }
        }
        System.out.println("Total: "+nr);
    }
}
