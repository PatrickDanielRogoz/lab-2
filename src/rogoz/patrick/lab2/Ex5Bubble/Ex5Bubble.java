package rogoz.patrick.lab2.Ex5Bubble;

import java.util.Random;

public class Ex5Bubble {
    public static void main(String[] args)
    {

        Random r = new Random();

        int[] a = new int[10];

        for(int i=0;i<a.length;i++)
            a[i] = r.nextInt(100);

        int n = a.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (a[j] > a[j+1])
                {
                    // swap arr[j+1] and arr[i]
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }

        for (int i=0;i<n;i++)
            System.out.println(a[i]+" ");




    }
}
